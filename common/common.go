package common

import (
	"bufio"
	"os"
)

func Check(e error) {
	if e != nil {
		panic(e)
	}
}

func Read(s string) (*bufio.Scanner, *os.File) {
	f, err := os.Open(s)
	Check(err)

	scanner := bufio.NewScanner(f)
	return scanner, f
}

func ReadBig() (*bufio.Scanner, *os.File) {
	return Read("../inputs/input-big.txt")
}

func ReadSmall() (*bufio.Scanner, *os.File) {
	return Read("../inputs/input-small.txt")
}

func MakeRange(min, max int) []int {
	a := make([]int, max-min+1)
	for i := range a {
		a[i] = min + i
	}
	return a
}
