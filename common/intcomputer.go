package common

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type IntComputer struct {
	Data    []byte
	Program []int
}

func (ic *IntComputer) GetZero() int {
	return ic.Program[0]
}

func (ic *IntComputer) Add(pos int, firstParam int, secondParam int, thirdParam int) (int, error) {
	if ic.Program[pos] != 1 {
		return pos, errors.New("Not right add opcode")
	}
	outputPos := thirdParam
	ic.Program[outputPos] = firstParam + secondParam
	return pos + 4, nil
}

func (ic *IntComputer) Multiply(pos int, firstParam int, secondParam int, thirdParam int) (int, error) {
	if ic.Program[pos] != 2 {
		return pos, errors.New("Not right multiply opcode")
	}
	outputPos := thirdParam
	ic.Program[outputPos] = firstParam * secondParam
	return pos + 4, nil
}

func (ic *IntComputer) Input(pos int, firstParam int) (int, error) {
	if ic.Program[pos] != 3 {
		return pos, errors.New("Not right input value")
	}

	outputPos := firstParam

	var input int
	fmt.Print("Enter input: ")
	_, err := fmt.Scan(&input)
	Check(err)

	ic.Program[outputPos] = input
	return pos + 2, nil
}

func (ic *IntComputer) Output(pos int, firstParam int) (int, error) {
	if ic.Program[pos] != 4 {
		return pos, errors.New("Not right output value")
	}

	outputPos := firstParam

	fmt.Print("Output: ")
	fmt.Println(ic.Program[outputPos])

	return pos + 2, nil
}

func (ic *IntComputer) LoadProgram(data []byte) {
	ic.Data = data
	stringSplit := strings.Split(string(data), ",")
	for _, v := range stringSplit {
		vInt, err := strconv.Atoi(v)
		Check(err)
		ic.Program = append(ic.Program, vInt)
	}
}

func (ic *IntComputer) PrintProgram() {
	for _, v := range ic.Program {
		fmt.Print(v)
		fmt.Printf(",")
	}
}

func (ic *IntComputer) PrintData() {
	fmt.Printf(string(ic.Data))
}

func (ic *IntComputer) ProcessMode(mode int, pos int, additionToPos int) int {
	param := 0
	if mode == 0 {
		param = ic.Program[ic.Program[pos+1+additionToPos]]
	} else if mode == 1 {
		param = ic.Program[pos+1+additionToPos]
	} else {
		panic("Not right mode")
	}

	return param
}

func (ic *IntComputer) ProcessParameterModes(pos int) (int, int, int, int, int) {
	instructionRaw := ic.Program[pos]
	if instructionRaw == 99 {
		return 99, 0, 0, 0, 0
	}

	instruction := instructionRaw % 10
	instructionRaw = instructionRaw / 10
	firstParam := instructionRaw % 10
	instructionRaw = instructionRaw / 10
	secondParam := instructionRaw % 10
	instructionRaw = instructionRaw / 10
	thirdParam := instructionRaw % 10

	firstParam = ic.ProcessMode(firstParam, pos, 0)
	secondParam = ic.ProcessMode(secondParam, pos, 1)
	thirdParam = ic.ProcessMode(thirdParam, pos, 2)
	thirdPos := ic.ProcessMode(1, pos, 2)

	return instruction, firstParam, secondParam, thirdParam, thirdPos
}

func (ic *IntComputer) Run() {
	indx := 0
	value, firstParam, secondParam, thirdParam, thirdPos := ic.ProcessParameterModes(indx)
	if thirdParam < -100 {
		fmt.Println("weird")
	}
	var err error
	for value != 99 {
		switch value {
		case 1:
			indx, err = ic.Add(indx, firstParam, secondParam, thirdPos)
			Check(err)
		case 2:
			indx, err = ic.Multiply(indx, firstParam, secondParam, thirdPos)
			Check(err)
		case 3:
			indx, err = ic.Input(indx, firstParam)
			Check(err)
		case 4:
			indx, err = ic.Output(indx, firstParam)
			Check(err)
		default:
			panic("Oh no")
		}
		ic.PrintProgram()
		value, firstParam, secondParam, thirdParam, thirdPos = ic.ProcessParameterModes(indx)
	}
}
