package main

import (
	"io/ioutil"

	"gitlab.com/TiffanyKalin/advent-go/common"
)

func main() {
	data, err := ioutil.ReadFile("../inputs/in-small.txt")
	common.Check(err)

	ic := &common.IntComputer{}
	ic.LoadProgram(data)
	ic.Run()
	ic.GetZero()
}
