package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/TiffanyKalin/advent-go/common"
)

func main() {
	min := 197487
	max := 673251

	possiblePasswords := 0
	for curr := min; curr <= max; curr = curr + 1 {
		s := strconv.Itoa(curr)
		ints := strings.Split(s, "")
		lastI, err := strconv.Atoi(ints[0])
		common.Check(err)
		lowestDouble := 10
		currDouble := 1

		common.Check(err)
		decreasing := true
		double := false
		for _, v := range ints[1:] {

			i, err := strconv.Atoi(v)
			common.Check(err)
			//double found

			if i == lastI {
				currDouble = currDouble + 1
				double = true

			} else if double == true {
				double = false
				if currDouble <= lowestDouble {
					lowestDouble = currDouble
				}
				currDouble = 1
			}

			if i < lastI {
				decreasing = false
			}
			lastI = i
		}
		if double && currDouble <= lowestDouble {
			lowestDouble = currDouble
		}
		if lowestDouble == 2 && decreasing {
			possiblePasswords = possiblePasswords + 1
			fmt.Println(curr)
		}
	}

	fmt.Println(possiblePasswords)
}

/*
x for range min-max:
	make x into an array
	loop through array:
		check for doubles
		check for non-decreasing

*/
