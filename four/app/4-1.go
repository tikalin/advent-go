package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/TiffanyKalin/advent-go/common"
)

func main() {
	min := 197487
	max := 673251

	possiblePasswords := 0
	for curr := min; curr <= max; curr = curr + 1 {
		s := strconv.Itoa(curr)
		ints := strings.Split(s, "")
		lastI, err := strconv.Atoi(ints[0])
		common.Check(err)
		decreasing := true
		double := false
		for _, v := range ints[1:] {
			i, err := strconv.Atoi(v)
			common.Check(err)
			if i == lastI {
				double = true
			}
			if i < lastI {
				decreasing = false
			}
			lastI = i
		}
		if double && decreasing {
			possiblePasswords = possiblePasswords + 1
			fmt.Println(curr)
		}
	}

	fmt.Println(possiblePasswords)
}

/*
x for range min-max:
	make x into an array
	loop through array:
		check for doubles
		check for non-decreasing

*/
