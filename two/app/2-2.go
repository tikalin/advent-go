package main

import (
	"fmt"
	"io/ioutil"

	"gitlab.com/TiffanyKalin/advent-go/common"

	"strconv"
	"strings"
)

func convertArray(data string) []int {
	var a []int
	stringSplit := strings.Split(string(data), ",")
	for _, v := range stringSplit {
		vInt, err := strconv.Atoi(v)
		common.Check(err)
		a = append(a, vInt)
	}

	return a
}

func main() {
	data, err := ioutil.ReadFile("../inputs/in-big.txt")
	common.Check(err)

	upper := common.MakeRange(0, 99)
	lower := common.MakeRange(0, 99)

	noun := -1
	verb := -1
	for _, i := range upper {
		for _, j := range lower {
			go func(i int, j int) {
				dataArray := convertArray(string(data))
				dataArray[1] = i
				dataArray[2] = j
				ic := &common.IntComputer{data, dataArray}
				//ic.PrintData()
				ic.Run()
				//ic.PrintProgram()
				//fmt.Println("")
				if ic.GetZero() == 19690720 {
					fmt.Println("here")
					noun = i
					verb = j
				}
			}(i, j)
		}
	}

	fmt.Println(noun)
	fmt.Println(verb)
	fmt.Println(100*noun + verb)

}
