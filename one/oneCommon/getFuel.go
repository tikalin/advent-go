package oneCommon

import "math"

func GetFuel(fuel int) float64 {
	return math.Floor(float64(fuel/3)) - 2
}
