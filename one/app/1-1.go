package main

import (
	"fmt"
	"strconv"

	"gitlab.com/TiffanyKalin/advent-go/common"
	one "gitlab.com/TiffanyKalin/advent-go/one/oneCommon"
)

func main() {
	scanner, file := common.ReadBig()
	defer file.Close()
	total := 0.0
	for scanner.Scan() {
		mass := scanner.Text()
		massInt, err := strconv.Atoi(mass)
		common.Check(err)
		total = total + one.GetFuel(massInt)
	}

	common.Check(scanner.Err())

	fmt.Println(total)

}
