package main

import (
	"fmt"
	"math"

	"gitlab.com/TiffanyKalin/advent-go/three/threeCommon"
)

func main() {
	s1, i1, s2, i2 := threeCommon.ReadLines()

	points, _ := threeCommon.ProcessLine(s1, i1, nil)
	_, intersection := threeCommon.ProcessLine(s2, i2, points)

	min := 100000.0
	steps := 0.0
	for idx, v := range intersection {
		steps = math.Abs(float64(v)) + math.Abs(float64(points[idx]))
		if steps < min {
			min = steps
		}
	}

	fmt.Println(min)
}
