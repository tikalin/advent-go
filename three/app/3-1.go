package main

import (
	"fmt"
	"math"

	"gitlab.com/TiffanyKalin/advent-go/three/threeCommon"
)

func main() {
	s1, i1, s2, i2 := threeCommon.ReadLines()

	points, _ := threeCommon.ProcessLine(s1, i1, nil)
	_, intersection := threeCommon.ProcessLine(s2, i2, points)

	min := 100000.0
	for p, _ := range intersection {
		manhattan := math.Abs(float64(p.X)) + math.Abs(float64(p.Y))
		if manhattan < min {
			min = manhattan
		}
	}

	fmt.Println(min)
}
