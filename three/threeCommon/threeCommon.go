package threeCommon

import (
	"strconv"
	"strings"

	"gitlab.com/TiffanyKalin/advent-go/common"
)

func ReadLine(line string) ([]string, []int) {
	var s []string
	var i []int

	stringSplit := strings.Split(line, ",")

	for _, v := range stringSplit {
		s = append(s, string(v[0]))
		num, err := strconv.Atoi(v[1:])
		common.Check(err)
		i = append(i, num)
	}

	if len(s) != len(i) {
		panic("Not matching")
	}
	return s, i
}

func ReadLines() ([]string, []int, []string, []int) {
	scanner, file := common.ReadBig()
	scanner.Scan()
	common.Check(scanner.Err())
	s1, i1 := ReadLine(scanner.Text())
	scanner.Scan()
	common.Check(scanner.Err())
	s2, i2 := ReadLine(scanner.Text())

	file.Close()

	return s1, i1, s2, i2
}

type Point struct {
	X int
	Y int
}

func ProcessLine(s []string, i []int, oldPoints map[Point]int) (map[Point]int, map[Point]int) {
	firstPass := true
	if oldPoints != nil {
		firstPass = false
	}
	points := make(map[Point]int)
	intersection := make(map[Point]int)
	currLocation := Point{0, 0}
	steps := 0
	for idx, v := range s {
		length := i[idx]
		newX := 0
		newY := 0
		switch v {
		case "R":
			newX = 1
			newY = 0
		case "L":
			newX = -1
			newY = 0
		case "U":
			newX = 0
			newY = 1
		case "D":
			newX = 0
			newY = -1
		}
		for length > 0 {
			steps = steps + 1
			currLocation = Point{currLocation.X + newX, currLocation.Y + newY}
			if firstPass {
				points[currLocation] = steps
			} else {
				if _, found := oldPoints[currLocation]; found {
					intersection[currLocation] = steps
				}
			}
			length = length - 1
		}
	}

	return points, intersection
}
